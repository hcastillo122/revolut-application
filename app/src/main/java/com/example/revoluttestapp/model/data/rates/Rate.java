package com.example.revoluttestapp.model.data.rates;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.revoluttestapp.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Represent a Rate.
 */
public class Rate implements Comparable<Rate> {
    private String mCurrencyCode;
    private double mValue, mShowValue;
    private Integer mViewOrder;

    /**
     * Constructor
     *
     * @param currencyCode
     * @param value
     */
    public Rate(Context context, String currencyCode, double value) {
        this.mCurrencyCode = currencyCode;
        this.mValue = value;
        this.mShowValue = value;
        SharedPreferences sharedPreference = context.getSharedPreferences(
                context.getString(R.string.shared_preference_key), Activity.MODE_PRIVATE);

        mViewOrder = sharedPreference.getInt(mCurrencyCode, 0);
    }

    /**
     * Constructor
     */
    public Rate() {
        super();
    }

    // Getters
    public String getCurrencyCode() {
        return mCurrencyCode;
    }

    public Integer getViewOrder() {
        return mViewOrder;
    }

    public void setViewOrder(Integer mViewOrder) {
        this.mViewOrder = mViewOrder;
    }

    public double getValue() {
        return mValue;
    }

    public void setValue(double mValue) {
        this.mValue = mValue;
    }

    public double getmShowValue() {
        return mShowValue;
    }

    public void setmShowValue(double mShowValue) {
        this.mShowValue = mShowValue;
    }

    public String getCurrencyName() {
        Map<String, String> values = new HashMap<>();
        values.put("EUR", "Euro");
        values.put("AUD", "Australian Dollar");
        values.put("BGN", "Bulgarian Lev");
        values.put("BRL", "Brazilian Real");
        values.put("CAD", "Canadian Dollar");
        values.put("CHF", "Switzerland Franc");
        values.put("CNY", "China Yuan/Renminbi");
        values.put("CZK", "Czech Koruna");
        values.put("DKK", "Denmark Krone");
        values.put("GBP", "Great Britain Pound");
        values.put("HKD", "Hong Kong Dollar");
        values.put("HRK", "Croatia Kuna");
        values.put("HUF", "Hungarian forint");
        values.put("IDR", "Indonesian Rupiah");
        values.put("ILS", "Israeli Shekel");
        values.put("INR", "Indian Rupee");
        values.put("ISK", "Icelandic Krona");
        values.put("JPY", "Japanese Yen");
        values.put("KRW", "South Korean Won");
        values.put("MXN", "Mexican Peso");
        values.put("MYR", "Malaysian Ringgit");
        values.put("NOK", "Norwegian Krone");
        values.put("NZD", "New Zealand Dollar");
        values.put("PHP", "Philippine Peso");
        values.put("PLN", "Polish Zloty");
        values.put("RON", "Romanian Leu");
        values.put("RUB", "Russian Ruble");
        values.put("SEK", "Swedish Krona");
        values.put("SGD", "Singapore Dollar");
        values.put("THB", "Thai Baht");
        values.put("TRY", "Turkish Lira");
        values.put("USD", "US Dollar");
        values.put("ZAR", "South African Rand");

        return values.get(mCurrencyCode);
    }


    public int getCurrencyImageId() {
        Map<String, Integer> values = new HashMap<>();
        values.put("EUR", R.drawable.ic_eur);
      values.put("USD", R.drawable.ic_eud);
        values.put("AUD", R.drawable.ic_aud);
        values.put("BGN", R.drawable.ic_bgn);
        values.put("BRL", R.drawable.ic_brl);
        values.put("CAD", R.drawable.ic_cnd);
        values.put("CHF", R.drawable.ic_chf);
        values.put("CNY", R.drawable.ic_cny);
        values.put("CZK", R.drawable.ic_czk);
//        values.put("DKK", R.drawable.ic_launcher_background);
//        values.put("GBP", R.drawable.ic_aud);
//        values.put("HKD", R.drawable.ic_aud);
//        values.put("HRK", R.drawable.ic_aud);
//        values.put("HUF", R.drawable.ic_aud);
//        values.put("IDR", R.drawable.ic_aud);
//        values.put("ILS", R.drawable.ic_aud);
//        values.put("INR", R.drawable.ic_aud);
//        values.put("ISK", R.drawable.ic_auda);
//        values.put("JPY", R.drawable.ic_aud);
//        values.put("KRW", R.drawable.ic_aud);
//        values.put("MXN", R.drawable.ic_aud);
//        values.put("MYR", R.drawable.ic_aud);
//        values.put("NOK", R.drawable.ic_aud);
//        values.put("NZD", R.drawable.ic_aud);
//        values.put("PHP", R.drawable.ic_aud);
//        values.put("PLN", R.drawable.ic_aud);
//        values.put("RON", R.drawable.ic_aud);
//        values.put("RUB", R.drawable.ic_aud);
//        values.put("SEK", R.drawable.ic_aud);
//        values.put("SGD", R.drawable.ic_aud);
//        values.put("THB", R.drawable.ic_aud);
//        values.put("TRY", R.drawable.ic_aud);
//        values.put("ZAR", R.drawable.ic_aud);

        return values.containsKey(mCurrencyCode) ? values.get(mCurrencyCode) : R.drawable.ic_launcher_background;
    }


    @Override
    public int compareTo(Rate o) {
        return this.getViewOrder().compareTo(o.getViewOrder());
    }
}
