package com.example.revoluttestapp.model.data.rates;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Class holder to have all rate information.
 */
public class RatesData {
    private String base;
    private String date;
    private Map<String, Double> rates;

    // Getters and setters
    public String getBase() {
        return base;
    }

    public String getDate() {
        return date;
    }

    public List<Rate> getRates(Context context) {
        List<Rate> ratesList = new ArrayList();

        if (rates != null) {
            for (Map.Entry<String, Double> entry : rates.entrySet()) {
                ratesList.add(new Rate(context, entry.getKey(), entry.getValue()));
            }
        }
        return ratesList;
    }
}
