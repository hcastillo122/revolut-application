package com.example.revoluttestapp.model.data.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.revoluttestapp.R;
import com.example.revoluttestapp.model.data.rates.Rate;

import java.text.DecimalFormat;
import java.util.List;

/***
 * Adapter class object that creates views for items in the dataSource.
 */
public class RatesAdapter extends RecyclerView.Adapter<RatesAdapter.ViewHolder> {
    private List<Rate> mDataSource;
    private int mBasePosition;
    private int pivotPosition;
    private OnRatesClickListener mOnRatesClickListener;
    private OnEditTextClickListener mOnEditTextClickListener;

    /**
     * To move the item from N to base position.
     */
    public interface OnRatesClickListener {
        void onClickItem(int position, List<Rate> rates);
    }

    /**
     * to change all the data
     */
    public interface OnEditTextClickListener {
        void onClick(int position, Editable editable, EditText editText,
                     TextWatcher textWatcher, List<Rate> mDataSource);
    }

    /**
     * Constructor
     *
     * @param dataSource              List with the rate information
     * @param onRatesClickListener    callback on click in the row.
     * @param onEditTextClickListener callback on click in the edit text.
     */
    public RatesAdapter(int basePosition, List<Rate> dataSource, OnRatesClickListener onRatesClickListener,
                        OnEditTextClickListener onEditTextClickListener) {
        mBasePosition = basePosition;
        mDataSource = dataSource;
        mOnRatesClickListener = onRatesClickListener;
        mOnEditTextClickListener = onEditTextClickListener;
    }

    @NonNull
    @Override
    public RatesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.rate_row, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RatesAdapter.ViewHolder viewHolder, final int position) {
        final Rate rate = mDataSource.get(position);

        if (rate.getCurrencyCode().equals("EUR")) {
            pivotPosition = viewHolder.getAdapterPosition();
        }

        viewHolder.textTitle.setText(rate.getCurrencyCode());
        viewHolder.textDescription.setText(rate.getCurrencyName());
        viewHolder.imageFlag.setImageResource(rate.getCurrencyImageId());
        viewHolder.currency.setText(getCurrentValueAsString(rate.getValue(), position));
        viewHolder.currency.setSelection(viewHolder.currency.length());
        viewHolder.setIsRecyclable(false);

        if (mOnRatesClickListener != null) {
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnRatesClickListener.onClickItem(viewHolder.getAdapterPosition(), mDataSource);
                }
            });
        }

        if (mOnEditTextClickListener != null) {
            viewHolder.currency.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    // callback listeners
                    mOnEditTextClickListener.onClick(viewHolder.getAdapterPosition(), editable,
                            viewHolder.currency, this, mDataSource);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mDataSource.size();
    }

    /**
     * Notify that data set has changed
     *
     * @param dataSource List with the new data to show.
     */
    public void notifyDataSetChanged(final List<Rate> dataSource) {
        mDataSource = dataSource;

        this.notifyDataSetChanged();
    }

    /**
     * Notify when the user move to base position.
     *
     * @param fromPosition original position
     * @param toPosition   position to move the row.
     */
    public void notifyItemMoveToBase(int fromPosition, int toPosition) {
        // update data array
        Rate item = mDataSource.get(fromPosition);
        mDataSource.remove(fromPosition);
        mDataSource.add(toPosition, item);

        this.notifyItemMoved(fromPosition, toPosition);
    }

    /**
     * Get the corresponding rate value with the amount on the first row
     *
     * @param value    money value.
     * @param position position in the recycler view.
     * @return String with two digit.
     */
    private String getCurrentValueAsString(double value, int position) {
        if (mBasePosition != position) {
            value *= mDataSource.get(mBasePosition).getValue();
        }

        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        return String.valueOf(decimalFormat.format(value));
    }

    /***
     * Inner class to provide a reference to each data item.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageFlag;
        TextView textTitle, textDescription;
        EditText currency;

        ViewHolder(View view) {
            super(view);

            imageFlag = view.findViewById(R.id.image_flag_icon);
            textTitle = view.findViewById(R.id.text_country_title);
            textDescription = view.findViewById(R.id.text_description);
            currency = view.findViewById(R.id.edit_value);
        }
    }
}
