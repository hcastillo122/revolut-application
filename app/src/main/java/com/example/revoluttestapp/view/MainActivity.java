package com.example.revoluttestapp.view;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.revoluttestapp.R;
import com.example.revoluttestapp.model.client.HttpVolleyClient;
import com.example.revoluttestapp.model.data.adapters.RatesAdapter;
import com.example.revoluttestapp.model.data.rates.Rate;
import com.example.revoluttestapp.model.data.rates.RatesData;
import com.google.gson.Gson;

import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final int BASE_POSITION = 0;
    public static final int DELAY_IN_MILLI = 1000;

    private Handler mHandler;
    private Runnable mRunnable;
    private RecyclerView mRecyclerView;
    private RatesAdapter mRatesAdapter;
    private SharedPreferences mPreferences;
    private Double mBaseValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initializer the recycler view.
        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));

        // Initializer sharePreference.
        mPreferences = getSharedPreferences(getString(R.string.shared_preference_key), MODE_PRIVATE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        startScheduleRatesJob();
    }

    @Override
    protected void onPause() {
        super.onPause();

        stopRatesJob();
    }

    /**
     * Stop the handler treat that GET the rate data.
     */
    private void stopRatesJob() {
        mHandler.removeCallbacks(mRunnable);
    }

    /**
     * Schedule task for GET the rate data for the server.
     */
    private void startScheduleRatesJob() {
        final HandlerThread handlerThread = new HandlerThread(getString(R.string.name_schedule_thread));
        handlerThread.start();

        mRunnable = new Runnable() {
            public void run() {
                HttpVolleyClient.OnReposeReceiveData onReposeReceiveData =
                        new HttpVolleyClient.OnReposeReceiveData() {
                            @Override
                            public void onReceiveData(String response) {
                                List<Rate> rates = handlerResponse(response);
                                if (mRatesAdapter == null) {
                                    setAdapter(rates);
                                } else {
                                    Collections.sort(rates);
                                    mRatesAdapter.notifyDataSetChanged(rates);
                                }
                            }

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(MainActivity.this,
                                        getString(R.string.message_error), Toast.LENGTH_LONG).show();
                            }
                        };

                HttpVolleyClient client = HttpVolleyClient.getInstance(getApplicationContext());
                client.getRatesValuesFromServer(onReposeReceiveData);

                mHandler.postDelayed(this, DELAY_IN_MILLI);
            }
        };

        mHandler = new Handler(handlerThread.getLooper());
        mHandler.post(mRunnable);
    }

    /**
     * Set Adapter with the rate information.
     *
     * @param dataSource list of rate
     */
    private void setAdapter(List<Rate> dataSource) {
        // set callback listeners
        final RatesAdapter.OnRatesClickListener onClickListener =
                new RatesAdapter.OnRatesClickListener() {
                    @Override
                    public void onClickItem(int position, List<Rate> rates) {
                        moveToFirst(position, rates);
                    }
                };

        final RatesAdapter.OnEditTextClickListener onEditTextListener =
                new RatesAdapter.OnEditTextClickListener() {
                    @Override
                    public void onClick(final int position, final Editable editable,
                                        final EditText editText, final TextWatcher textWatcher,
                                        final List<Rate> dataSource) {

                        stopRatesJob();

                        mRecyclerView.post(new Runnable() {
                            @Override
                            public void run() {
                                if (editable != null && position == BASE_POSITION) {
                                    // Checking editable.hashCode() to understand which edit text is using right now
                                    if (editText.getText().hashCode() == editable.hashCode()) {

                                        editText.removeTextChangedListener(textWatcher);

                                        String value = editable.toString().equalsIgnoreCase("") ? "0" : editable.toString();
                                        mBaseValue = Double.valueOf(value);

                                       // editText.setText(value);
                                        dataSource.get(BASE_POSITION).setValue(mBaseValue);
                                        mRatesAdapter.notifyDataSetChanged(dataSource);

                                        //editText.setText(value);
                                        editText.setSelection(editable.length());
                                        editText.addTextChangedListener(textWatcher);
                                    }
                                }
                            }
                        });

                        startScheduleRatesJob();
                    }
                };


        Collections.sort(dataSource);
        mRatesAdapter = new RatesAdapter(BASE_POSITION, dataSource, onClickListener, onEditTextListener);
        mRecyclerView.setAdapter(mRatesAdapter);
    }

    /**
     * Move the row to the base position in the adapter.
     *
     * @param position position in the adapter.
     * @param rates    List of rate values.
     */
    private void moveToFirst(int position, List<Rate> rates) {
        if (position != BASE_POSITION) {
            // Update SharePreference position
            final SharedPreferences.Editor editor = mPreferences.edit();

            for (int i = 0; i <= position; i++) {
                if (position == rates.get(i).getViewOrder()) {
                    rates.get(i).setViewOrder(BASE_POSITION);
                } else {
                    rates.get(i).setViewOrder(rates.get(i).getViewOrder() + 1);
                }

                editor.putInt(rates.get(i).getCurrencyCode(), rates.get(i).getViewOrder());
                editor.apply();
            }

            // Move item and notify adapter
            mRatesAdapter.notifyItemMoveToBase(position, BASE_POSITION);
            mRecyclerView.scrollToPosition(BASE_POSITION);
        }
    }

    /**
     * Handler response String with the JSON Object and parse the data to show.
     *
     * @param response Response data with json.
     * @return List of Rate object.
     */
    private List<Rate> handlerResponse(String response) {
        RatesData data = new Gson().fromJson(response, RatesData.class);

        List<Rate> dataSource = data.getRates(getApplicationContext());
        dataSource.add(BASE_POSITION, new Rate(getApplicationContext(), data.getBase(),
                mBaseValue != null ? mBaseValue : 100));

        if (mRatesAdapter == null) {
            //Put data in share preference.
            final SharedPreferences.Editor editor = mPreferences.edit();

            for (int i = 0; i < dataSource.size(); i++) {
                editor.putInt(dataSource.get(i).getCurrencyCode(), i);
            }
            editor.apply();
        } else {
            for (int i = 0; i < dataSource.size(); i++) {
                dataSource.get(i).setViewOrder(mPreferences.getInt(
                        dataSource.get(i).getCurrencyCode(), 1));
            }
        }
        return dataSource;
    }
}
